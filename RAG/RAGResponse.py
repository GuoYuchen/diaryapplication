import os
# 导入OpenAI模块，用于与OpenAI语言模型交互
from langchain.llms import OpenAI
# 导入PromptTemplate模块，用于创建和管理提示模板
from langchain.prompts import PromptTemplate
# 导入ChatOpenAI类，用于创建和管理OpenAI聊天模型的实例
from langchain.chat_models import ChatOpenAI
# 导入TextLoader类，用于从文本文件加载数据
from langchain_community.document_loaders import TextLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
# 导入VectorstoreIndexCreator类，用于创建和管理向量存储索引
from langchain.indexes import VectorstoreIndexCreator
# 导入dotenv库，用于加载.env文件中的环境变量
from dotenv import load_dotenv
from langchain.embeddings import CacheBackedEmbeddings
from langchain_community.vectorstores import FAISS
from langchain.storage import LocalFileStore
from langchain.llms.openai import OpenAIChat
from langchain.chains import RetrievalQA
from langchain.callbacks import StdOutCallbackHandler

# 直接设置 OpenAI API 密钥
os.environ["OPENAI_API_KEY"] = 'the openai api key'

#文件路径
file_path =  '/Users/guoyuchen/Documents/DiaryFile/Mysqltest1Diary.txt'

#使用TextLoader加载文件
document_loader = TextLoader(file_path)

#加载文件内容
document_content = document_loader.load()

# 加载.env文件中的环境变量
load_dotenv()

# 使用TextLoader加载文本文件，指定文件路径和编码格式
loader = TextLoader('/Users/guoyuchen/Documents/DiaryFile/Mysqltest1Diary.txt', encoding='utf8')

# 使用VectorstoreIndexCreator从加载的数据创建向量存储索引
index = VectorstoreIndexCreator().from_loaders([loader])

# 定义要查询的字符串
query = "What things will make people feel unhappy?"

# 使用创建的索引执行查询
result = index.query(query)

# 打印查询结果
print(result)

