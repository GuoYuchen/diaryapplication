import pymysql
import datetime
import os

#数据库连接配置
db_config = {
        'host': "diaryapplicationdemo.cwtvfmjokt9c.us-west-1.rds.amazonaws.com",
        'user': "admin",
        'password': "admin123",
        'database': "diaryapplication"
}

#连接数据库
connection = pymysql.connect(**db_config)


#文件路径,保存在当前路径下
file_path = '/Users/guoyuchen/Documents/DiaryFile/Mysqltest1Diary.txt'

#检查文件是否存在
if not os.path.exists(file_path):
    #如果文件不存在,先从数据库查询该用户之前所有的diarynote
    with connection.cursor() as cursor:
        #查询该用户所有的diarynote
        cursor.execute("SELECT diarynote FROM Diary WHERE username = 'Mysqltest1'")
        all_results = cursor.fetchall()

        #如果查询到结果,写入到新创建的文件中
        if all_results:
            #'w':将文件以写入模式打开
            with open(file_path, 'w') as new_file:
                for row in all_results:
                    new_file.write(f"{row[0]}\n")
                    print("past diarynote has been inserted.")

#监听并继续插入
try:
    with connection.cursor() as cursor:
        #根据username查询,并监听新日记的插入
        sql = "SELECT diarynote FROM Diary WHERE username = %s AND date > %s"

        #设定起始时间,这里是脚本运行时刻
        last_check = datetime.datetime.now()

        while True:
            #更新查询时间
            current_check = datetime.datetime.now()
            #执行查询
            cursor.execute(sql, ('Mysqltest1', last_check))
            #提取结果
            results = cursor.fetchall()

            #如果有新数据,追加到文件中
            if results:
                #'a': 如果文件已存在，新写入的内容将会被追加到文件的末尾，而不是覆盖原有内容
                with open(file_path, 'a') as file:
                    for row in results:
                        file.write(f"{row[0]}\n")
                        print("new diarynote has been inserted.")

            #更新最后检查时间
            last_check = current_check

finally:
    connection.close()
